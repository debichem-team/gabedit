Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Gabedit
Upstream-Contact: Abdul-Rahman ALLOUCHE <allouchear@users.sourceforge.net>
Source: https://sourceforge.net/projects/gabedit/files/gabedit/
Comment: This package was debianized by Daniel Leidert <daniel.leidert@wgdd.de>
 on Thu, 2 Sep 2004 23:12:54 +0200.
Disclaimer: Please use the following citations in any report or publication:
 Gabedit — A graphical user interface for computational chemistry software.
 Allouche, A.-R. , Journal of Computational Chemistry, 32 (2011) 174–182.
 .
 DOI: 10.1002/jcc.21600 <https://dx.doi.org/10.1002/jcc.21600>

Files: *
Copyright: 2002-2021 Abdul-Rahman Allouche
License: Expat

Files: gl2ps/gl2ps.c gl2ps/gl2ps.h
Copyright: 1999-2006 Christophe Geuzaine <geuz@geuz.org>
License: LGPL-2+ or GL2PS-License
 This program is free software; you can redistribute it and/or
 modify it under the terms of either:
 .
 a) the GNU Library General Public License as published by the Free
 Software Foundation, either version 2 of the License, or (at your
 option) any later version; or
 .
 b) the GL2PS License as published by Christophe Geuzaine, either
 version 2 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See either
 the GNU Library General Public License or the GL2PS License for
 more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library. On Debian systems, a copy of the
 GNU Library General Public License (LGPL) version 2 can be found
 at '/usr/share/common-licenses/LGPL-2'.
 .
 A full copy of the GL2PS License, version 2 is below.
 .
 For the latest info about gl2ps, see http://www.geuz.org/gl2ps/.
 Please report all bugs and problems to <gl2ps@geuz.org>.
 .
 On Debian systems, a copy of the GNU Library General Public License (LGPL)
 version 2 can be found at /usr/share/common-licenses/LGPL-2.
 .
 The full text of the GL2PS license is:
 .
                           GL2PS LICENSE
                      Version 2, November 2003
 .
              Copyright (C) 2003, Christophe Geuzaine
 .
 Permission to use, copy, and distribute this software and its
 documentation for any purpose with or without fee is hereby granted,
 provided that the copyright notice appear in all copies and that both
 that copyright notice and this permission notice appear in supporting
 documentation.
 .
 Permission to modify and distribute modified versions of this software
 is granted, provided that:
 .
 1) the modifications are licensed under the same terms as this
 software;
 .
 2) you make available the source code of any modifications that you
 distribute, either on the same media as you distribute any executable
 or other form of this software, or via a mechanism generally accepted
 in the software development community for the electronic transfer of
 data.
 .
 This software is provided "as is" without express or implied warranty.

Files: debian/*
Copyright: 2004-2021 The debichem team <debichem-devel@lists.alioth.debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the Gabedit), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.
